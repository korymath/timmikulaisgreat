// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
  'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
  'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
  'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
  'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
          console[method] = noop;
        }
      }
    }());

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-27870079-3', 'timmikulaisgreat.com');
ga('send', 'pageview');

function justImage() {
	var imageNum = Math.ceil(Math.random() * 9);
	document.getElementById('great-image').src = "img/".concat(imageNum.toString()).concat(".jpg");
}

function getQuote() {
	var mySheet = "https://spreadsheets.google.com/feeds/list/0AnGsv23Fy_89dFBOS3I5YklXanBlLVlVNF9SeFA3QVE/od6/public/values?alt=json";
	$.getJSON(mySheet, function(data) {
   var maxNum = data.feed.openSearch$totalResults.$t;
   var curIndex = Math.ceil(Math.random() * maxNum);
   var imageNum = Math.ceil(Math.random() * 9);
   $('#rn').html(curIndex.toString());
   $('#quote').fadeOut(800, function() {
     $('#quote').html(stripHTML(data.feed.entry[curIndex].gsx$reason.$t));
     $("#quote").fadeIn(800);
   });
   $('#great-image').fadeOut(800, function() {
     document.getElementById('great-image').src = "img/".concat(imageNum.toString()).concat(".jpg");
     $("#great-image").fadeIn(800);
   });
 });    
}

//i'm removing the HTML because otherwise the element gets added as BLOCK, which will be instantly visible and you will not get the 'fadeIn' look
function stripHTML(html) {
 var tmp = document.createElement("DIV");
 tmp.innerHTML = html;
 return tmp.textContent||tmp.innerText;
}

function getMaxNum() {
	var mySheet = "https://spreadsheets.google.com/feeds/list/0AnGsv23Fy_89dFBOS3I5YklXanBlLVlVNF9SeFA3QVE/od6/public/values?alt=json";
	$.getJSON(mySheet, function(data) {
   var maxNum = data.feed.openSearch$totalResults.$t;
   
   document.getElementById('maxNum').innerHTML = maxNum.toString();
 });   	
}