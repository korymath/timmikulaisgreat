Tim Mikula is Great - The Website
================

Tim Mikula is a really great dude, I think so, but so does the entire internet. I decided to see exactly why the internet thinks he is so great.

Sources
--------

I posted simple request and a photo of you to several online outsourcing markets (Amazon's Mechanical Turk and CrowdFlower), then I sifted through the hundreds of responses and these are the best ones.

What started off as a simple idea has since ballooned into a huge database of reasons why Tim Mikula is great. Currently there are over 100 reasons logged, and you can submit your own!

Technology
----------

All the reasons are stored in a public Google Drive Spreadsheet, and the site randomly pulls a reason using Asynchronous JavaScript and JavaScript Object Notation.

The site is built on the HTML5 Boilerplate using Initializr, it is hosted on Bluehost, and the entire source code is free and open on GitHub so feel free to fork it, track and issue, or add a pull request.

The site is continuoulsy deployed using Dploy.io, and Grunt with UglifyJS and other awesome JS tools. Analytics are of course handled by Google Analytics.
