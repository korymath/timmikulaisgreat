module.exports = function(grunt) {

// Project configuration.
grunt.initConfig({

  pkg: grunt.file.readJSON('package.json'),

  uglify: {
    build: {
      src: [
      'js/vendor/jquery-1.10.1.min.js',
      'js/vendor/bootstrap.min.js',
      'js/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
        'js/plugins.js'], //inputs
      dest: 'js/build/global.min.js' //outputs
    }
  },

  sass: {                              // Task
    dist: {                            // Target
      options: {                       // Target options
        style: 'compressed'
      },
      files: {                         // Dictionary of files
        'css/build/main.css': 'css/main.scss',       // 'destination': 'source'
      }
    }
  },

  watch: {
    options: {
      livereload: true,
    },
    css: {
      files: ['css/main.scss'],
      tasks: ['sass']
    }
  },

  connect: {
    all: {
      options: {
        base: './'
      }
    }
  }

});

grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-contrib-connect');
grunt.loadNpmTasks('grunt-contrib-watch');

grunt.registerTask('default',['uglify','sass','connect']);

grunt.registerTask('dev',['uglify','sass','connect','watch']);

};